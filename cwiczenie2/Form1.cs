﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cwiczenie2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGeneruj_Click(object sender, EventArgs e)
        {
            string klucz = "";
            int liczbaZnakow = 0;
            try
            {
                int dlugoscKlucza = int.Parse(txtDlugosc.Text);

                Random liczbaLosowa = new Random();
                while (liczbaZnakow < dlugoscKlucza)
                {
                    int cyfra = liczbaLosowa.Next(0, 9);
                    klucz += cyfra.ToString();
                    liczbaZnakow++;
                    if (liczbaZnakow % 5 == 0 && liczbaZnakow < dlugoscKlucza)
                    {
                        klucz += "-";
                    }
                }
                txtKlucz.Text = klucz;
                MessageBox.Show("Klucz wygenerowany pomyślnie", "Sukces", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } catch(Exception ex)
            {
                MessageBox.Show("Błąd w trakcie generacji", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
