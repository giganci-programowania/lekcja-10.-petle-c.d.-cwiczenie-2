﻿namespace cwiczenie2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGeneruj = new System.Windows.Forms.Button();
            this.txtDlugosc = new System.Windows.Forms.TextBox();
            this.txtKlucz = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnGeneruj
            // 
            this.btnGeneruj.Location = new System.Drawing.Point(209, 31);
            this.btnGeneruj.Name = "btnGeneruj";
            this.btnGeneruj.Size = new System.Drawing.Size(75, 23);
            this.btnGeneruj.TabIndex = 0;
            this.btnGeneruj.Text = "Generuj";
            this.btnGeneruj.UseVisualStyleBackColor = true;
            this.btnGeneruj.Click += new System.EventHandler(this.btnGeneruj_Click);
            // 
            // txtDlugosc
            // 
            this.txtDlugosc.Location = new System.Drawing.Point(139, 33);
            this.txtDlugosc.Name = "txtDlugosc";
            this.txtDlugosc.Size = new System.Drawing.Size(64, 20);
            this.txtDlugosc.TabIndex = 1;
            // 
            // txtKlucz
            // 
            this.txtKlucz.Location = new System.Drawing.Point(139, 64);
            this.txtKlucz.Name = "txtKlucz";
            this.txtKlucz.Size = new System.Drawing.Size(224, 20);
            this.txtKlucz.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Długość klucza";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Wygenerowany klucz";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 117);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtKlucz);
            this.Controls.Add(this.txtDlugosc);
            this.Controls.Add(this.btnGeneruj);
            this.Name = "Form1";
            this.Text = "Generator CD-KEY";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGeneruj;
        private System.Windows.Forms.TextBox txtDlugosc;
        private System.Windows.Forms.TextBox txtKlucz;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

